1.0.0 - May 4, 2015

PSR-7 Acceptance


0.11.0 - May 4, 2015

  - Clarifications to RequestInterface::withUri() specifying that changes MUST
    also update the Host header unless the $preserverHost flag is true.
  - Removal of the various getHeader*() method overrides in RequestInterface,
    as they are irrelevant with the above change.
  - Added verbiage to the RequestInterface class-level docblock indicating
    that constructors SHOULD attempt to set the Host header in order to ensure
    the message is valid on initialization.
  - RequestInterface::getHeaderLine() MUST return a string (null was allowed
    previously, which leads to ambiguity of return value).
  - ResponseInterface::getReasonPhrase() MUST return a string(null was allowed
    previously, which leads to ambiguity of return value).
  - ResponseInterface::withStatus() now sets the default value of the
    $reasonPhrase parameter to an empty string (to correspond to the above
    change).
  - UploadedFileInterface::move() was renamed to moveTo(), and the argument
    renamed to $targetPath; this clarifies intent for the method.
  - UriInterface::withPath() has clarifications in the docblock to indicate
    the four possible path types (empty, root path, relative path,
    domain-relative — or absolute — path).


0.10.1 - Apr 13, 2015

Clarify RequestInterface::getUri() return value


0.10.0 - Apr 13, 2015

  - StreamableInterface was renamed to StreamInterface; typehints were updated
    accordingly.
  - ServerRequestInterface::*FileParams() were renamed to *UploadedFiles().
    They now expect and return array trees of a new interface,
    UploadedFileInterface.
  - UploadedFileInterface was added to provide compatibility between SAPI and
    non-SAPI environments with regards to handling upload files.
  - A number of clarifications were made on UriInterface with regards to URI
    component delimiters.


0.9.2 - Mar 18, 2015

Interface clarifications


0.9.1 - Mar 4, 2015

URI path/query encoding and Request Host header clarifications


0.9.0 - Feb 12, 2015

Use the verbiage 'parsed body' instead of 'body params'.


0.8.0 - Jan 28, 2015

Reference request-target only, not the request line


0.7.0 - Jan 28, 2015

Model URIs only, and move alternate request targets to Request's request-line


0.6.0 - Jan 18, 2015

Immutability + URI interface
